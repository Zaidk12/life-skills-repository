# My major take away from each of the sections

From **Gathering requirements section**

- My take is while discussing something with the team we need to take notes on important topics. The questions we ask should be clear and detailed.

From **Always over-communicate: Some scenarios section**

- My take is needed to communicate to your team members as if requirements are little communicated to your team members. While asking questions on the platform using a public channel instead of DM's will solve your problem easily as lots of people may solve that problem before the one you're asking. Most importantly turning on your camera while in the meeting or so, it will improve communication.

From **Stuck? Ask question section**

- My take is to improve the way we use to ask questions we need to make it easy for the person we are asking the question. Trying asking questions with a detailed view of your question if you could use diagrams, code snippets, or image it will be good. 

From **Get to know your teammates**

- My take is we need to give more time in the office to meeting new people talking to team members play with them rather than working all the time taking a break is a good habit.
While talking to somebody be available don't let them wait for your reply it will not going to make your communication great.

From **Be aware and mindful of other team members section**

- My take is we avoid disturbing our team members or anybody while they were working we need to respect them as we respect our work. Communicate when they are free.

From **Doing things with 100% involvement section**

- My take is we need to give our 100% to our work. We need to take care of ourselves to improve our concentration and be healthy we need to eat healthy, sleep enough and exercise. So, that we can fully concentrate on our work.

# Which area do you think you need to improve on? What are your ideas to make progress in that area?

Some of areas i need to improve myself is :

1. I need to improve my habit of asking questions without knowing whether the person I'm asking for help is doing his/her work or free.

2. I need to post my question when I am stuck on public channels rather than on DM's.

3. I need to inform my every small change in the plan to my teams to let them if there is any change in the program.

4. I need to take some time to spend with friends and office colleagues to know them well and to improve my network.

5. I need to pay attention to any feedback or suggestions given by my teammates or my mentor.

6. I need to pay attention to every small detail of my projects or any kind of work I will do or doing.
