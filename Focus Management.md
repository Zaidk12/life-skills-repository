# Focus Management

**What is Deep Work**

Deep work is the ability to concentrate deeply on a difficult task for prolonged periods of time without getting distracted. It creates that intense, out-of-body kind of focus that makes you completely oblivious to what’s going on around you – the kind that produces your best work.

**Paraphrase all the ideas in the above videos and this one in detail.**

Speaker says that optimal duration for deep work is 60 to some more than 60 minutes . 

He also talks about deadline. He said that deadline is a motivational signal. Done the work before deadlines gives our minds sense of releif . Deadline gives the power to defeat our mind to do the work on or before the deadline.

Speaker defines deep work as professional activities performed in a state of distraction free concentration that pushes your cognitive abilities to their limit. 

These efforts create new value improve skill and hard to replicate. Because of deep work Harry potter has been wrote succesfully, Bill gate make the basic version of Microsoft code ,Writing new coding langauage is also a part of deep work because it is not easy to write a programming language.

**How can you implement the principles in your day to day life?**


There are ways I can try to implement these principles :

* First of all i will be remove the distracting elements from my working area.

* I do my work for 60 min in full focus mode then i will reward my self with 10 min break.

* Sleep properly so, that i can do my work with full focus.

* Leave any kind of screen 1 hour before going to bed.

* In morning time our mind gets in focus mode quickly, so I will study/ work in morning time.

**Your key takeaways from the video**

If social media is used in a clueless way, this can have emotional, social, financial, and even legal consequences. In some cases, it can even lead to personal data being shared. Children and teenagers are especially exposed to social media risks, but this doesn’t mean that adults, authorities, banks, and even large internet companies are not immune. Social media is not a fundamental technology so we have to use it. Social media sites are a business, in which they are taking our every movement's records and selling it to others to make money from them. These social media wants to addict users with their social sites so that we user can spend more of our time with those sites and by collecting our every moment record they will make money. They even hire attention engineers so that users can spend more time with their social sites. This is the desired use case of these social media sites.

Social media apps and websites have the same kind of effect on the brain as playing a slot machine. Since you don’t know the content you’ll see until you open the app, the spontaneous results cause a feeling of “reward” by releasing dopamine.

If you will lose your ability to sustain concentration, you're going to become less and less relevant to this economy. There is also psychological harm that the more you use social medial, the more likely you are to going to feel lonely or isolated.
