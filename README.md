# What is Full Text Search ?

Full-text search refers to searching some text inside extensive text data stored electronically and returning results that contain some or all of the words from the query. In contrast, traditional search would return exact matches. 
While traditional databases are great for storing and retrieving general data, performing full-text searches has been challenging. Frequently, additional tooling is required to achieve this.

## Full Text Search Examples

Full-text search can have many different usages—for example, looking for a dish on a restaurant menu or looking for a specific feature in the description of an item on an e-commerce website. In addition to searching for particular keywords, you can augment a full-text search with search features like fuzzy-text and synonyms. Therefore, the results for a word such as “pasta” would return not only items such as “Pasta with meatballs” but could also return items like “Fettuccine Carbonara” using a synonym, or “Bacon and pesto flatbread” using a fuzzy search.

## This is just the brief of Full Text Search. But we are here to investigate about Elasticsearch, Solr and Lucene.

# What is Elasticsearch ?

Elasticsearch is a highly scalable open-source full-text search and analytics engine. It allows you to store, search, and analyze big volumes of data quickly and in near real time. It is generally used as the underlying engine/technology that powers applications that have complex search features and requirements. 

Elastic search is standing as a NOSQL DB beacuse :
 
- It easy-to-used
- Has a great community
- Compatibility with JSON
- Broad use cases

# Use cases of Elasticsearch

- Application search
- Website Search
- Enterprise Search 
- Logging and Log analytics
- Security analytics 
- Business analytics

## For simplicity i take three main companies for use cases 

# Netflix 

Netflix messaging system behind the scene is using Elasticsearch. The messaging system is divided in the following categories:

- The message you receive when you join the service.

- Once people joined they receive messages about the content they might enjoy or new feature on the server.

- Once they know more about you through Machine Learning algorithms, they send more engaging and personalized messages about what you
might like or enjoy to watch.

- In case you decide to leave the service they tell you how to come back.

This is all done through emails, app push notifications, and text messages. To accomplish that in an efficient way they need to know almost instantly about possible issues in the delivery of the message. For this reason Elasticsearch was introduced (previously they were using distributed grep) for message life cycle.
In a nutshell each status message is recorded on Elasticsearch and the proper team is able to filter each category by writing a query on Kibana.

# Tinder

Tinder at its core is a search engine. The search queries are complex with double digits events, hundreds countries, and more than 50 languages. Most users interactions trigger an Elasticsearch query.
There are different ways to interact with tinder based on locations. For example in Asia they also use it as a language exchange or to search for a tour guide.
For this reason the queries in Tinder are very complex. They must be:

- **Personalized** : Machine Learning algorithms are also utilized in this context. 

- **Locatuion based** : To find a match based on where you are at a certain point in time. 

- **Bidirectional** : To know which users will swipe right on each other, which basically means a match. 

- **Real Time** : The entire interaction has to happen within milliseconds from a massive amount of users and with many variables associated with each of them.

Considering all these functionalities the backend reality is very complex broadening from data science and machine learning, to bidirectional ranking and geolocation. Elasticsearch cornerstone is to make those components work together in a very efficient way.

# Cisco Commerce Delivery Platform

Elasticsearch was introduced in 2017 when they upgraded their commercial platform. They switched from RDBMS to Elasticsearch for the following reasons:

- Add Fault Tolerance working in active/active mode. RDBMS are not distributed and are not fault tolerant.

- Rank based and type ahead Search for data sourced from multiple Databases on 30/40 attributes to get sub-seconds responses.

- Global search: if no specific objects are specified in your search, the search engine will find results against multiple objects.

# How it works 

To better understand Elasticsearch and its usage is good to have a general understanding of the main backend components.

## Node 
A node is a single server that is part of a cluster, stores our data, and participates in the cluster’s indexing and search capabilities. Just like a cluster, a node is identified by a name which by default is a random Universally Unique Identifier (UUID) that is assigned to the node at startup. We can edit the default node names in case we want to.

## Cluster 
A cluster is a collection of one or more nodes that together holds your entire data and provides federated indexing and search capabilities. There can be N nodes with the same cluster name.
Elasticsearch operates in a distributed environment: with cross-cluster replication, a secondary cluster can spring into action as a hot backup.

## Index
The index is a collection of documents that have similar characteristics. For example, we can have an index for a specific customer, another for a product information, and another for a different typology of data. An index is identified by a unique name that refers to the index when performing indexing search, update, and delete operations. In a single cluster, we can define as many indexes as we want. Index is similiar to database in an RDBMS.

## Document 
A document is a basic unit of information that can be indexed. For example, you can have an index about your product and then a document for a single customer. This document is expressed in JSON (JavaScript Object Notation) which is a ubiquitous internet data interchange format. Analogy to a single raw in a DB.
Within an index, you can store as many documents as you want, so that in the same index you can have a document for a single product, and yet another for a single order.

## Shard and Replicas
Elasticsearch provides the ability to subdivide your index into multiple pieces called shards. When you create an index, you can simply define the number of shards that you want. Each shard is in itself a fully-functional and independent “index” that can be hosted on any node in the cluster.
Shards is important cause it allows to horizontally split your data volume, potentially also in multiple nodes paralelizing operations thus increasing performance. Shards can also be used by making multiple copies of your index into replicas shards, which in cloud environments could be useful to provide high availability.

# Conclusion

Elasticsearch is a distributed, RESTful and analytics search engine capable of solving a wide variety of problems.
Many companies are switching to it and integrating it in their current backend infrastructure since:

 - It allows to zoom out to your data using aggregation and make sense of billions of log lines.

 - It combines different type of searches: structured, unstructured, Geo, application search, security analytics, metrics, and logging.

 - It uses standard RESTful APIs and JSON. The community has also built and maintains clients in many languages such as Java, Python, .NET, SQL, Perl, PHP etc.

 - It is possible to put the real-time search and analytics features of Elasticsearch to work on your big data by using the Elasticsearch-Hadoop (ES-Hadoop) connector.

 # Apache Solr Search

 Splr is an open-source enterprise-search platform, written in Java. Its major features include full-text search, hit highlighting, faceted search, real-time indexing, dynamic clustering, database integration, NoSQL features and rich document (e.g., Word, PDF) handling. Providing distributed search and index replication, Solr is designed for scalability and fault tolerance. Solr is widely used for enterprise search and analytics use cases and has an active development community and regular releases. 

 ## Features of Solr Search

 Solr is a standalone enterprise search server with a REST-like API. You put documents in it (called "indexing") via JSON, XML, CSV or binary over HTTP. You query it via HTTP GET and receive JSON, XML, CSV or binary results. 

 There are many more features other than written below. But i mentioned only the most benificial features of Apache Solr below :
 
 **Advanced Full-Text Search Capabilities** : Solr enables powerful matching capabilities including phrases, wildcards, joins, grouping and much more across any data type

**Optimized for High Volume Traffic** : Solr is proven at extremely large scales the world over.

**Easy Monitoring** : Need more insight into your instances? Solr publishes loads of metric data via JMX .

**Flexible and Adaptable with easy configuration** : Solr's is designed to adapt to your needs all while simplifying configuration.

**Powerful Extensions** : Solr ships with optional plugins for indexing rich content (e.g. PDFs, Word), language detection, search results clustering and more.

**Geospatial Search** : Enabling location-based search is simple with Solr's built-in support for spatial search.

**Performance Optimizations** : Solr has been tuned to handle the world's largest sites.

**Rich Document Parsing** : Solr ships with Apache Tika built-in, making it easy to index rich content such as Adobe PDF, Microsoft Word and more.

# How it works 

In order to search a document, Apache Solr performs the following operations in sequence: 

**Indexing** : Converts the documents into a machine-readable format.

**Querying** : Understanding the terms of a query asked by the user. These terms can be images or keywords.

**Mapping** : Solr maps the user query to the documents stored in the database to find the appropriate result.

**Ranking** : As soon as the engine searches the indexed documents, it ranks the outputs by their relevance.

# Use cases and applications of Apache Solr

Solr is a search engine with multiple uses that has proven critical to business operations. Besides the powerful search features, Solr makes for an exceptional data store for analytics use. Solr is, therefore, the backbone used for applications with sophisticated search and analytics requirements in any domain, really, from marketing, energy, education to HR, healthcare, retail, real estate and many more.
Some solr use cases are :

**Text Analytics**
Hiring managers of recruitment agencies have to scan piles of resumes to find just a few suitable candidates to interview. Solr can help reduce the time spent on going through resumes. With Apache Tika, it can index unstructured data coming from rich text documents such as PDFs, Word documents, XML, or plain text. The search engine can pull keywords and phrases, identify and convert different word forms, and detect the language that was used.

**Spatial Analytics**
When expanding a store chain, Solr can help strategic planners decide where the new location should be. Using its geospatial functionality, it can map out the existing and potential customers and include distance as criteria when ranking each potential location. Additionally, by analyzing customer purchases, it can group customers by distance traveled, number of visits, or the amount purchased.

**Log File Analytics**
A perfect example of how Solr can support very large indexes is to see it in action in manufacturing. In this kind of operation, parts are tracked from the moment they enter the inventory until they leave the line fully assembled. And not just once, but by every machine they pass through on the assembly line. Solr can handle this massive amount of data and provide efficient ingestion and search capabilities in near real-time. With Solr, you can easily see production rate, defect rate, group data by date range, product line, location, etc.

# Companies using Apache solr in there products:

## Linkdin 
LinkedIn, a well known professional social media site, uses Lucene/Solr search. Lucene has a powerful faceting system that allows us to pivot and navigate by user or company attributes abstracted from user profile data. LinkedIn has an excellent feature that is backed up by Solr: its ranking of results by people's relationship with you. This data is not fixed, and being derived by Lucene in real time, it's all based on the arithmetic calculations of the relationships in your connections list.

## Flipkart 
Flipkart is a leading example of Solr. It has more than 900k users and sees more than 20k searches per second. Flipkart product search has a backbone of 175 million listings, ~250 million documents, and ~5,500 categories. The major challenge was real-time results, ranking, autocompletion, high-update rates, and inverted index. It has become a huge success by using Solr for product searches for its e-commerce business.

## Netflix
Netflix uses Solr for the site search feature. Netflix has more than 2 million queries per day for searches and more than 15 million subscribers. It is available in more than 190 countries and supports around 23 languages. The search works based on video title name, genre name, or person name. Features such as autocompletion and ranked results are used by Netflix.

# Conclusion

Apache Solr is the backbone of any Enterprise which needs to incorporate the Search platform into its application. It has uses in almost all major industries and therefore the possibilities are endless and although it’s touted as a search platform it can perform analytical tasks with great complexity and with a user interface that’s second to none. Therefore, learning Solr along with other technologies like Hadoop and Big data Analytics is imperative for anyone looking for an interesting career in Data Science or ‘Search’ in any major Tech Companies.

# Apache Lucene

Apache Lucene is a high-performance, full-featured search engine library written entirely in Java. It is a technology suitable for nearly any application that requires structured search, full-text search, faceting, nearest-neighbor search across high-dimensionality vectors, spell correction or query suggestions.

# How it works 

**Acquire Raw Content** : The first step of any search application is to collect the target contents on which search application is to be conducted.

**Build the document** : The next step is to build the document(s) from the raw content, which the search application can understand and interpret easily.

**Analyze the document** : Before the indexing process starts, the document is to be analyzed as to which part of the text is a candidate to be indexed. This process is where the document is analyzed.

**Indexing the document** : Once documents are built and analyzed, the next step is to index them so that this document can be retrieved based on certain keys instead of the entire content of the document. Indexing process is similar to indexes at the end of a book where common words are shown with their page numbers so that these words can be tracked quickly instead of searching the complete book.

**User Interface for Search** : Once a database of indexes is ready then the application can make any search. To facilitate a user to make a search, the application must provide a user a mean or a user interface where a user can enter text and start the search process.

**Build Query** : Once a user makes a request to search a text, the application should prepare a Query object using that text which can be used to inquire index database to get the relevant details.

**Search Query** : Using a query object, the index database is then checked to get the relevant details and the content documents.

**Render Results** : Once the result is received, the application should decide on how to show the results to the user using User Interface. How much information is to be shown at first look and so on.

# Why Lucene needed 

Search is one of the most common operations we perform multiple times a day. This search can be across multiple web-pages which exist on the Web or a Music application or a code repository or a combination of all of these. One might think that a simple relational database can also support searching. This is correct. Databases like MySQL support full-text search. But what about the Web or a Music application or a code repository or a combination of all of these? The database cannot store this data in its columns. Even if it did, it will take an unacceptable amount of time to run the search this big.

A full-text search engine is capable of running a search query on millions of files at once. The velocity at which data is being stored in an application today is huge. Running the full-text search on this kind of volume of data is a difficult task. This is because the information we need might exist in a single file out of billions of files kept on the web.

# Benefits of using Apache Lucene

**Speed** : Lucene that works entirely on Java performs at a sub-second for most queries to deliver results, enhancing organizational efficiency.

**Complete query capability** : The search technology encompasses everything from spell-checking proximity operators to enabling multi-lingual search.

**Holistic results ** : Lucene performs a full-result processing that includes relevancy-sorting, sorting using date or any given field and also dynamic summaries.

**Probability** :  Lucene runs on just about any platform that is compatible with Java. What’s more, its indexes are portable across platforms too.

# Components of Lucene

**Directories** : A Lucene index stores data in normal file system directoies or in memory if you need more performance. It is completely the apps choice to store data wherever it wants, a Database, the RAM or the disk.

**Documents** : The data we feed to the Lucene engine needs to be converted to plain-text. To do this, we make a Document object which represents that source of data. Later, when we run a search query, as a result, we will get a list of Document objects that satisfy the query we passed.

**Fields** : Documents are populated with a collection of Fields. A Field is simply a pair of (name,value) items. So, while creating a new Document object we need to fill it with that kind of paired data. 

**Terms** : Terms represents a word from the text. Terms are extracted from the analysis and tokenization of Fields’ values, thus Term is the smallest unit on which the search is run.

**Analyzers** : An Analyzer is the most crucial part of indexing and searching process. It is the Analyzer which conevrts the plain-text into Tokens and Terms so that they can be searched.


# Reference 

From Google about Full Text Search [Full Text Search] (https://www.mongodb.com/basics/full-text-search)

From Youtube about Elasticsearch [Elasticsearch] (https://www.youtube.com/watch?v=-WF2fQFZ-Uk&t=264s)

From Google about Elasticsearch [Elasticsearch] (https://www.knowi.com/blog/what-is-elastic-search/)

From Google about Apache Solr [Apache Solr] (https://solr.apache.org/)

From Google about Apache Lucene [Apache Lucene] (https://lucene.apache.org/)
















 





