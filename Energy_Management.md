# Enery Management 

## Question 1 

**What are the activities you do that make you relax - Calm quadrant?**

Basically, whenever i play cricket or travel or ride bike these are some activities i do to feel calm and relax. Other than that i play video games and watch youtube to be relax and calm . Like when i done any task while working i gave my self 5-10 min break in that break i watch youtube or instagram that help me feel relaxed and give tendancy to do more work.

## Question 2

**When do you find getting into the Stress quadrant?**

There are some time i find my self in that zone :

1. When my work is not completed and i got new work at that time i find myself in stressed form.
2. When i am doing something and someone asking me something again and again that makes me angry.
3. When my work is not completed and deadline of the work is near than it makes me feel tensed.

## Question 3

**How do you understand if you are in the Excitement quadrant?**

Whenever i done my work before time or i am going somewhere great or my team is about to win a game or whenever i acheived something then these are the moments when i find myself in the Excited zone.

## Question 4

**Paraphrase the Sleep is your Superpower video in detail**

In the video, the speaker discusses the impacts of sleeping for less time and tips to sleep well.

He says that sleep distortions have an impact on age, and he further says that sleep distortion has an impact on reproductive health as well. He says that students have to sleep well before studying to learn better and after too to retain better. He suggests that one has to have eight hours of sleep every day to have good health. He has shown some experimental results which show that people with good sleep have good retention power than those who have not sleep well and said that it will double the memory power.

Speaker warned about the cardiovascular effect due to lack of sleep and showed data on how heart attacks increase from the lack of sleep. He also added that lack of sleep may kill our  cells that protects us from getting any disease which eventually leads to some form of cancer at some point in life. Even WHO warned about night work in MNC comapies.

He also suggest some tips to sleep better some of the points are we need to regulate our sleep time and wake up time . The temperature of the room should be modarate not so cool not so hot.  He also added that sleep is not optional sleep is necessary we have to sleep 7-8 hours a day to keep our body and brain working properly. 

## Question 5

**What are some ideas that you can implement to sleep better?**

1.  Avoid sleeping during daytime because it will contribute to breaak our sleep regularity at night
2.  Our room temperature is used to be modarate
3.  Using breathing exercises
4.  Having a regular sleep pattern
5.  Listening to relaxing music & read something
6.  Writing dairy before bed which will remove all negative things from mind   
7.  Limiting caffeine 

## Question 6

**Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

In this video, the speaker highlights the importance of doing exercises.
Doing regular exercises will
1.  Increases focus on what we do
2.  Increases neuro transmitters in our brain
3.  Keeps us in good shape that increase our confidence
4.  Increases reaction time
5.  Produces new brain cells
6.  Improves long-term memory
7.  Keeps us in good mood to work well

## Question 7

**What are some steps you can take to exercise more?**

1. Walk instead of drive, whenever you can.
2. Take the stairs instead of the escalator or elevator.
3. Take a family walk after dinner.
4. Replace a Sunday drive with a Sunday walk.
5. Go for a half-hour walk instead of watching TV.
6. Get off the bus a stop early, and walk.

