# Tiny Habits

## Question 1

**Your takeaways from the video (Minimum 5 points)**

My takeaway from the video is :

1. We all need to made tiny changes in our behaviour.
2. We need to add tiny habits along with our daily habits.
3. Tiny habits if we follow consistently will draw a big change in our life.
4. We need to compliment ourselves after we complete any tiny habit.
5. We should tell our friends and family members to also add some tiny habits in their life as well.

## Question 2

**Your takeaways from the video in as much detail as possible**

My takeaways from the video is  :

The universal formula for human behavior is: B = MAP (a behavior happens when motivation, ability, and a prompt converge at the same
moment).
If you’re developing a new habit (like reading a book every night before bed), you will perform the habit if your motivation (desire to read)
matches the ability required to read (access to a book and time needed to hit your daily reading goal) the moment you receive a prompt to
read (get a reminder on your phone or see a book on your nightstand).

Anyone can hack the universal behaviour model by just following "Tiny Habits Method" given below :

**Part #1: Shrink the Behavior**

If task is hard , then you need high levels of motivation to rise abovet the "action line" and complete the task.

Fogg suggested shrinking every habit to the tiniest possible version, so you don't need to rely on motivation. We can find the tiniest version of our desired habit by either reducing the quantity or doing just the first step.

**Part #2: Identify an action prompt**

There are three types of habit prompts (habit reminders):

1. External/context prompts: cues from your environment like post‐it notes, phone notifications, and alarms.

2. Internal prompts (also known as personal prompts): thoughts and sensations that remind you to act, like a grumbling stomach.

3. Action prompts: the completion of one behavior reminds you to start the next behavior. Loading the dishwasher can be a
prompt to clean the kitchen countertops.

External and internal prompts are distracting and de‐motivating. If you use an alarm to prompt your new habit, you’ll need to stop what
you’re doing and pivot to your new habit. But if you use an action prompt, momentum will carry you from one behavior to the next, and
you’ll need less motivation to start your new habit. There are many action prompts you can use to perform a thirty second habit.

**Part #3: Grow Your Behavior with Some Shine**

“Shine” is a term BJ Fogg created to explain the feeling you get after an accomplishment. The closest thing in English language is authentic
pride. “You know this feeling already: You feel Shine when you ace an exam. You feel Shine when you give a great presentation and
people clap at the end. You feel Shine when you smell something delicious that you cooked for the first time.” – BJ Fogg
You MUST generate the feeling of Shine after you execute your tiny habit if you want to sustain and grow your habit. It may sound
ridiculous to feel pride and success after doing one pushup or flossing one tooth but learning to celebrate tiny wins is the most critical
component of habit development (based on BJ Fogg’s extensive habit develop research).
When you give yourself a steady dose of Shine after doing the tiniest version of a habit, your motivation will steadily grow. When your
motivation increases, you move higher up the “action line” and can tackle harder habits.


## Question 3

**How can you use B = MAP to make making new habits easier?**

According to BJ Fogg, the founder of the Behavior Design Lab at Stanford University, a behavior happens when motivation, ability, and a prompt converge simultaneously. This is known as the Fogg Behavior Model or B=MAP.

If you want to change your life, you’ll have to change your behaviors. There are only three simple variables that drive those behaviors. That’s where the B=MAP model comes in. It is the framework to understand and unlock the mystery of how habits take root in routines. It will help you adopt helpful habits and get rid of unhelpful ones. This post outlines the B=MAP model and lays down steps how you can apply it in your life to make room for transformative change.

**The B=MAO Model**

Behavior is the result of motivation, ability, and prompts taken at the same time. This is a simple formula that can lead to incredible results. In other words, a behavior is the result of:

- Motivation, or your desire to execute the behavior.
- Ability, i.e. your capacity to execute the behavior.
- Prompt, or your cue to execute the behavior.

**Motivation**

Most people wrongly assume that motivation is all you need to successfully build a new habit. Fogg disagrees, stating that motivation is only one part of the equation. And as we all know. motivation is unreliable. It doesn’t last forever.

According to Fogg, there are three sources of motivation:

- Yourself, i.e. what you know you want.
- A reward or punishment you know you would receive if you complete the behavior.
- The context, i.e. if people around you are doing it as well.

**Ability**

If you’re able to execute a behavior, you’ll not need much motivation to continue doing it.

How can I make this behavior easier to do? There are only three answers to this, according to Fogg’s research:

1. **Increase your skills.** Research more on the habit you’re focussing. You should do this immediately when your motivation is high. This can be done by reading books, watching tutorial videos, or joining a group of likeminded people.

2. Get tools and resources that help you with the new habit.


3. **Make the behavior tiny.** Focus on the starter steps — one small habit you can do every day that will lead you towards your desired behavior. In case you are trying ot break a habit, you should taking the behavior you want and shrink it, one step at a time.

**Prompts**

Prompts are the triggers or cues that push you to start the habit. As per Fogg’s research, there are three types of prompts:



1. **Person Prompts:** When you rely on a prompt from within to start a behavior, for example, a sore back might prompt you to stand up and stretch. While useful, such prompts are unlikely to lead to lasting change as they are so unreliable.

2. **Context Prompts:** These are cues in your environment that urge you to take action, for example, an alarm clock or a notification on your phone. They are useful, but having too many to manage can lead to overwhelm.

3. **Action Prompts:** These are the most reliable type of prompts, and as Fogg defines, “a behavior you already do that can remind you to do a new habit you want to cultivate.” For example, an existing habit of making coffee for yourself ever morning can serve as a prompt to take your medications.

![txt]("https://cdn-images-1.medium.com/max/1000/0*I-dcVRzvYTLv2_HO.jpg")

In Fogg’s own words: “When a behavior is prompted above the Action Line, it happens. Suppose you have high motivation but no ability (you weigh 120 pounds, but you want to bench-press 500 pounds). You’re going to fall below the Action Line and feel frustrated when you are prompted. On the other hand, if you are capable of the behavior but have zero motivation, a prompt won’t get you to do the behavior; it will only be an annoyance. What causes the behavior to be above or below the line is a combination of motivation pushing up and the ability to move you to the right. Here’s a key insight: Behaviors that ultimately become habits will reliably fall above the Action Line.”


## Question 4

**Why it is important to "Shine" or Celebrate after each successful completion of habit?**

“Shine” is a term BJ Fogg created to explain the feeling you get after an accomplishment. The closest thing in English language is authentic
pride. “You know this feeling already: You feel Shine when you ace an exam. You feel Shine when you give a great presentation and
people clap at the end. You feel Shine when you smell something delicious that you cooked for the first time.” – BJ Fogg
You MUST generate the feeling of Shine after you execute your tiny habit if you want to sustain and grow your habit. It may sound
ridiculous to feel pride and success after doing one pushup or flossing one tooth but learning to celebrate tiny wins is the most critical
component of habit development (based on BJ Fogg’s extensive habit develop research).
When you give yourself a steady dose of Shine after doing the tiniest version of a habit, your motivation will steadily grow. When your
motivation increases, you move higher up the “action line” and can tackle harder habits.

## Question 5

**Your takeaways from the video (Minimum 5 points)**

My takeaways from the videos are:

1. If you want to be successfull you just don't need to focus of the your success but instead you need to see the process.

2. Getting better every day doesn’t means any value of 1% but that 1% should make big impact on your life.

3. While developing new habit make habit that helps you improve your life style, self esteem and most importantly self confidence.

4. Every action is a vote for the type of person you wish to become. Means every action that you take while developing your self to your desired form that takes you closer to your desirable person you want to be.

5. One should optimized for the starting line instead of the finising line. Person need to be optimized for starting any work rather than finishing it.

## Question 6

**Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?**

1. Don't start the habit-changing process by concentrating on goals. This prompts us to develop behavior-based habits. Creating identity-based behaviours is an alternative. With this strategy, we begin by concentrating on the person we want to become.

2. Big transformations can't be achieved in a single day, follow small daily routines or complete the task in parts it will help us complete our task and give chance to learn new things.

## Question 7

**Write about the book's perspective on how to make a good habit easier?**

- We have to have a craving for learning good habits and stick to them. 

- We need to be responsive while learning new habits.

- We will definitely be rewarded with a better lifestyle when we give time to learn good habits

## Question 8

**Write about the book's perspective on making a bad habit more difficult?**

You should make distance yourself from the bad behavior less distance from the good one.

## Question 9 

**Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

I want to be polite with every person I meet and get them to be comfortable as they can so that the conversation should be more powerful and satisfying. I ensure that my tone should be moderate, My body language should be open and my behavior should be friendly.

## Question 10

**Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

The habit that I want to eliminate is getting attached to phones and I want to limit the use of phones in my daily life. For that fixed the time when I need to use the phone and when I don't need to use the phone and I keep myself busy so, that I get less free time because I get free time I will definitely use my phone to waste it. 

