# Learning Process

# How to learn faster with the Feyman Technique

## What is Feyman Technique

The Feynman Technique is a method of learning that unleashes your potential and forces you to develop a deep understanding. 

**There are four different  ways you can adopt to learn anything:**

1. Choose a concept you want to learn about. Write it down on piece of paper.

2. Explain the concept using simple language

3. Identify problem areas , where you are weak, then stress more to learn that topics first.

4. Explain your concept to a kid as kids.

Basically, this video summaries the ways given by the great scientist Richard Feynman.
To learn anything completely and effictivily. In this video the person explain the four techniques anyone apply on their daily basis learning process to get outstanding
results on their learning and catching the concept.

## The different ways to implement these techniques on myself is:

**Way1**: Like i get any topic from my mentor to learn then i will firstly take little detail about it like how much i need to learn on that topics how much i need to leave. After that i learn in down anywhere like in phone or on paper. After that it will be simple for me to learn content serial wise from that very topic.

**Way2** : As i get any topic to learn i will breakdown the topic in simple language as it do not make the topic complicated for me to understand and after that i will follow the first step to learn it very effectively.

**Way3** : The most effective way to learn anything is to teach that exact topic to anyone of your friend, your colleague or any kid at home . This technique will definitely help us to learn our topic. The person may ask questions like how this works what this function do. Then, It will definitely help you streanthen your knowledge on that exact topic.

# Learning how to learn TED talk by Barbara Oakley

## Paraphrasing the video

Barbara Oakley trying to explain the diffrent ways to learn new things with the example of greatest techniques that was used by the greatest people of their times like scientists, engineers and others.

She also tells how she became a computer science professor at university by applying those great techniques on their to learn and catch concepts very easily and effectively. 

She talks about how she implements the diffrent techniques to change her brain to grasp more and more knowledge. 

She tells that she met many professor and people who is successful on their respective feild she ask them how they learn complex things and also explain to others and after asking so many people she finds out that our brain work in two different modes. The modes are focused mode and diffused mode.

She also quoted the example of Thomas Edition how he also used both the modes to solve their experiment problems.
 
She talks about procrastination as well .She said procrastination once or twice is ok but more often it will cause problem. 
  
She also talks about pomodoro technique and how can we use it to rid of procrastination and use our times well fully focused. Basically, the pomdoro technique estates that your do your with fully focus for 25 minutes after that do something fun. This way you can enhanced your productivity and focus towards your work.

## What are some of the steps that you can take to improve your learning process?

- Most important steps to improve learning is to figure out the topic you choose to learn

- Secondly use the power of focused mode and diffused mode of our brain.

- We can use our time well. For that we can use pomodoro technique to best use our time.

- You don't need to compare yourself with others it does not matter if others are better than you on that.

# Learn Anything in 20 hours

## Paraphrase your understanding.

In the video Josh Kaufman tells us about how can we can learn anything in 20 hours if we can use our time best and practice, practice and practice.

He tells about the stereotype of learning anything in 10000 hours. He tell us that it is not true that you take 10000 hours to learn new skills but if you want to be expert on that feild then maybe it takes that much time.

The most important thing we need to keep in mind while learning anything is we need to practice alot.

He tells 4 simple steps to follow to learn anything in 20 hours means you have to give 45 minutes of focused time.

The methods are deconstruct the skill, learn enough to self-correction, remove practice barriers and the last one is practice atleast 20 hours.

## What are some of the steps that you can while approaching a new topic?

The things i usually done while approaching to any new topic is first i searched some good quality videos for that topic on youtube (good means content wise good) after that i search about that topic on google like if want to learn any coding topic then firstly i search for video after that i search it on google to get good documentation or any website demonstrate it in easy way . And this habit benefits me alot while learning topics.
Other way to learn new topic is to discuss with my friends who already done that topic or atleast have some knowledge about it. In that way it is easier for me understand the topic in my own language and i can ask any questions i want unlike youtube tutorial and other online resources.




