# Grit

**Paraphrase (summarize) the video in a few lines. Use your own words.**

- Grit is passion and perseverance for long-term and meaningful goals.
- Many of the successful people, good students have one thing common in them that is grit.
- Grit is a personality that make people never give up on their passion or work.
- Grit is related to mindset in that if one believes that failures are due to their fixed traits, there is no reason to try again. Conversely, individuals with growth mindset are more likely to be resilient and have more grit.

**What are your key takeaways from the video to take action on?**

My takeaways from the videos are :

- Grit makes people to stick to their dream or passion and worked really hard to acheive that goal.
- Grit gave people never gave up attitude.
- Grit make you do good on your own feild.

# Growth Mindset

**Paraphrase (summarize) the video in a few lines in your own words.**

A fixed mindset means you believe intelligence, talent, and other qualities are innate and unchangeable. If you’re not good at something, you typically think you will never be good at it. By contrast, a growth mindset means you believe intelligence and talent can be developed with practice and effort. Not surprisingly, your mindset plays a major role in your motivation, resilience, and achievement.

**What are your key takeaways from the video to take action on?**

My takeaways from the videos are :

- You are not just one or the other. This is a spectrum . In diffrent time in diffrent days in diffrent situation you might be in growth or in fixed . We have to identify when we are in fixed mindset and then fixed it.

- We only grow with our action of learning. We have to work on our mindset and work on beliefs and focus to make good growth mindset.  

#  Internal Locus of Control

**What is the Internal Locus of Control? What is the key point in the video?**

Locus of control is what an individual believes causes his or her experiences, and the factors to which that person attributes their successes or failures.  Internal locus of control is often used synonymously with "self-determination" and "personal agency. Experts have found that, in general, people with an internal locus of control tend to be better off.

**Key Points**

- There two types of locus of control :  1. Internal Locus of control 2. External Locus of control.

- We talked about internal locus of control above.

- External locus of control anchors one end of a continuum of the locus of control construct with the other end anchored by internal locus of control. 

- Do your works sincerely and appreciate your self after completing the work.

# Build a Growth Mindset

**Paraphrase (summarize) the video in a few lines in your own words.**

If you feel that a growth mindset is something you want to aim for, there are ways you can go about developing one. However, it’s important to recognise that, no one has an entirely fixed or entirely growth mindset; most are somewhere in the middle.
There are two kind of people, one with the fixed mindset and other will be of growth mindset. Fixed mindset people are those people who believe that they don't grow they don't believe that they get better and they believe that talent is god given. Growth mindset people think to develop such an ability, we have to believe in our ability to solve a problem which helps in lifelong development and growth. If we have a big dream and still don't know how to achieve it, we must think in such a way that we will find a way to achieve it. 

**What are your key takeaways from the video to take action on?**

My takeaway from the video is :

- Identify your own mindset.
- Look at your own improvements.
- Review the success of others.
- Seek feedback
- Learn something new.
- Make mistakes and correct them.
- Be kind to yourself.
- Set realistic goals.

#  Mindset - A MountBlue Warrior Reference Manual

**What are one or more points that you want to take action on from the manual? (Maximum 3)**

1. I will stay with a problem till I complete it. I will not quit the problem.

2. I know more efforts lead to better understanding.

3. I will not write a word of code that I don’t understand.

4. I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.

5. I will stay relaxed and focused no matter what happens.



 
