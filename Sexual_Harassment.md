# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Sexual Harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or widespread and affects working condition or creates a disapproving work environment.

There are generally three forms of sexual harassment:

1. Verbal 
2. Visual
3. physical

**Verbal Harassment**: In verbal harassment when anybody crakes jokes regarding body, clothing, gender based jokes or requesting sexual favours in change of work done. These are some common behaviour of verbal sexual harassment.

**Visual Harassment**: In visual harassment includes obscene photos, inappropriate drawing or pictures, cartoons or emails of sexual nature comes under visual harassment.

**Physical Harassment**: Physical harassment includes sexual assaults, inappropriate touching such as kissing, hugging , rubbing, sexual gesturing . These comes under physical harassment.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

Firstly, I will tell them to stop what they are doing but if they don't stopped after repeatedly stopping by me then i will inform about this to mentor or hr . If it will not seems helping me then i will inform to higher authorities about that. Because work place need to be free from these kind of things. Friendly and positive environment is want a work place needs.
