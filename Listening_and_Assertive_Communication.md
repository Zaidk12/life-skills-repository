# Active Listening

## What are the steps/strategies to do Active Listening?
## Some important steps to do Active Listening:
- Don't get distracted while listening to someone.
- Try not to interrupt the speaker.
- You pharases that reflects your are interested in the topic.
- Show that you are listening by body language.
- Try taking notes of important conversation.

# Reflective Listening

Reflective listening is basically listening the ideas and thoughts of the speaker and then offering the idea back to the speaker, to confirm the idea has been understood correctly.

The key points of reflective listening is :

1. **Empathy**
2. **Acceptance**
3. **Congruence**
4. **Concreteness**

- **Empathy**
Empathy is the listener's desire and effort to understand the speaker in more depth. The empathic listeners tries to involve in conversation as much as possible . To understand the thoughts of the speaker really well.

- **Acceptance**
Acceptance is the realated to empathy. Acceptance means having respect for the speaker. And one need to control themselves from expressing agreement or disagreement with what the speaker says.

- **Congruence**
Congruence means openness, acceptance and genuineness on the part of the speaker. The Congruence  listener express their true feeling while listening to speakers rather than faking it.

- **Concreteness**
Concreteness means to focusing on specifics rather than vague feelings. Concreteness listener asks speakers to speak more specific rather than speaking vague and abstract.

# What are the obstacles in your listening process?

The main obstacle in my way to listening process is i am not able to maintain concentration for long time if speaker takes more time then i started start losing interest from the conversation. 

# What can you do to improve your listening?

I do many things to imporove my listening process:

1. Most importent thing i do is to listen to speaker without any distraction that helps me take more infomation from the conversion.

2. I try to involve in conversation so, that i do not get bored.

3. I try to repeat what i understand from conversion so, that i came to know i am good listener.

# When do you switch to Passive communication style in your day to day life?

I generally don't switched to passive communication but i do when i am with my old friends, when i am with my family, with the ones i respect alot.

# When do you switch into Aggressive communication styles in your day to day life?

I generally try to be in assertive communication but if any situation comes like i am trying to stop somebody for doing something that irritates me or distract me from doing my work then may be i switched to Aggressive communication but for the very short period of time.

# When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I swiched to passive aggressive communication while i am with my friends because i want to be more sarcastic around them and naturally i became sarcastic when i am with my friends.

# How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

Basically, If i want to make my communication assertive i have to look at these steps:

1. Firstly, We don't need to be aggresive we have to respect the feeling of others and want our work to be done as well.
2. We have to ask for your need and try to do more often.
3. If we have to tell our feeling to other then we need to first recognize and know our feelings to better convey it to others.
4. If we are telling anybody about our needs then also we need to know and we have to recogniize our needs .
5. If you starts practising assertive communication then you need to starts with small stakes like asking your friend to lower the sound of phone or asking your friend to bring you coffee while you are working etc.
6. We have to speak up about the situation that is problematic for us as early as it pining in our mind.
   
